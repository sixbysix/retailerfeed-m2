<?php
declare(strict_types=1);

namespace SixBySix\BeggRetailerFeed\Service;

use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;
use SixBySix\BeggRetailerFeed\Api\Service\SftpConnectionInterface;
use SixBySix\BeggRetailerFeed\Helper\ConfigHelper;

/**
 * Class SftpConnection
 */
class SftpConnection implements SftpConnectionInterface
{
    /** @var ConfigHelper */
    protected $configHelper;

    /** @var Filesystem */
    protected $filesystem;

    /**
     * SftpConnection constructor.
     * @param ConfigHelper $configHelper
     */
    public function __construct(ConfigHelper $configHelper)
    {
        $this->configHelper = $configHelper;
    }

    /**
     * @param string $filename
     * @return false|mixed|resource
     * @throws FileNotFoundException
     */
    public function download(string $filename)
    {
        return $this->getFilesystem()->readStream($filename);
    }

    /**
     * @param string $filename
     * @return bool
     * @throws FileNotFoundException
     */
    public function delete(string $filename): bool
    {
        return $this->getFilesystem()->delete($filename);
    }

    /**
     * @return Filesystem
     */
    protected function getFilesystem(): Filesystem
    {
        if ($this->filesystem === null) {
            $sftp = new SftpAdapter([
                'host' => $this->configHelper->getHost(),
                'port' => $this->configHelper->getPort(),
                'username' => $this->configHelper->getUsername(),
                'password' => $this->configHelper->getPassword(),
                'root' => '/',
            ]);
            $this->filesystem = new Filesystem($sftp);
        }

        return $this->filesystem;
    }
}
