<?php
declare(strict_types=1);

namespace SixBySix\BeggRetailerFeed\Model\Config\Source;

use Magento\Inventory\Model\SourceRepository;
use Magento\Inventory\Model\Source;
use SixBySix\BeggRetailerFeed\Api\Data\Config\Source\InventorySourceInterface;

/**
 * Class InventorySource
 */
class InventorySource implements InventorySourceInterface
{
    /** @var SourceRepository */
    protected $sourceRepository;

    public function __construct(SourceRepository $sourceRepository)
    {
        $this->sourceRepository = $sourceRepository;
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function toOptionArray()
    {
        $sourceList = $this->sourceRepository->getList()->getItems();
        $sourceOptions = [];

        /** @var Source $source */
        foreach ($sourceList as $source) {
            $sourceOptions[] = [
                'value' => $source->getSourceCode(),
                'label' => $source->getName(),
            ];
        }

        return $sourceOptions;
    }
}
