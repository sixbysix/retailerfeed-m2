<?php
declare(strict_types=1);

namespace SixBySix\BeggRetailerFeed\Cron;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory;
use SixBySix\BeggRetailerFeed\Api\Cron\InventoryFeedImportCronInterface;
use SixBySix\BeggRetailerFeed\Api\Helper\ConfigHelperInterface;
use SixBySix\BeggRetailerFeed\Api\Service\ConnectionInterface;

/**
 * Class InventoryFeedImportCron
 */
class InventoryFeedImportCron implements InventoryFeedImportCronInterface
{
    /**´
     * @var SourceItemInterfaceFactory
     */
    protected $sourceItemFactory;

    /**
     * @var SourceItemsSaveInterface
     */
    protected $sourceItemsSave;

    /** @var ConfigHelperInterface */
    protected $configHelper;

    /** @var ConnectionInterface */
    protected $remote;

    /** @var ProductRepositoryInterface */
    protected $productRepository;

    /**
     * InventoryFeedImportCron constructor.
     * @param ConfigHelperInterface $configHelper
     * @param ConnectionInterface $connection
     * @param SourceItemInterfaceFactory $sourceItemFactory
     * @param SourceItemsSaveInterface $sourceItemsSave
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ConfigHelperInterface $configHelper,
        ConnectionInterface $connection,
        SourceItemInterfaceFactory $sourceItemFactory,
        SourceItemsSaveInterface $sourceItemsSave,
        ProductRepositoryInterface $productRepository
    ) {
        $this->configHelper = $configHelper;
        $this->remote = $connection;
        $this->sourceItemFactory = $sourceItemFactory;
        $this->sourceItemsSave = $sourceItemsSave;
        $this->productRepository = $productRepository;
    }

    /**
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Validation\ValidationException
     */
    public function execute(): void
    {
        if (!$this->configHelper->isEnabled()) {
            return;
        }

        $csv = $this->getInventoryCsv();
        while ($row = fgetcsv($csv)) {

            if (sizeof($row) < 3) {
                throw new Exception("Invalid row received");
            }

            $sku = (string) $row[0];
            $price = (float) $row[1];
            $qty = (int) $row[2];
            $inStock = ($qty > 0) ? 1 : 0;

            try {
                // update product price
                $product = $this->productRepository->get($sku);
                $product->setPrice($price);
                $this->productRepository->save($product);

                // update product stock
                $sourceItem = $this->sourceItemFactory->create();
                $sourceItem->setSourceCode($this->configHelper->getInventorySource());
                $sourceItem->setSku($sku);
                $sourceItem->setQuantity($qty);
                $sourceItem->setStatus($inStock);
                $this->sourceItemsSave->execute([$sourceItem]);
            } catch (NoSuchEntityException $e) {
                // this sku isn't found, just skip rather than fail
                continue;
            }
        }

        // on success, delete the file
        $this->deleteInventoryCsv();
    }

    /**
     * @return false|resource
     */
    protected function getInventoryCsv()
    {
        return $this->remote->download($this->configHelper->getInventoryFilename());
    }

    /**
     * @return bool
     */
    protected function deleteInventoryCsv(): bool
    {
        return $this->remote->delete($this->configHelper->getInventoryFilename());
    }
}
