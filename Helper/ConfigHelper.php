<?php
declare(strict_types=1);

namespace SixBySix\BeggRetailerFeed\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use SixBySix\BeggRetailerFeed\Api\Helper\ConfigHelperInterface;

/**
 * Class ConfigHelper
 */
class ConfigHelper extends AbstractHelper implements ConfigHelperInterface
{
    const XML_ENABLED = 'begg_retailer_feed/general/enabled';
    const XML_USERNAME = 'begg_retailer_feed/sftp/username';
    const XML_PASSWORD = 'begg_retailer_feed/sftp/password';
    const XML_HOST = 'begg_retailer_feed/sftp/host';
    const XML_PORT = 'begg_retailer_feed/sftp/port';
    const XML_INVENTORY_FILENAME = 'begg_retailer_feed/files/inventory_filename';
    const XML_INVENTORY_SOURCE = 'begg_retailer_feed/general/inventory_source';

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->scopeConfig->getValue(
            self::XML_USERNAME,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->scopeConfig->getValue(
            self::XML_PASSWORD,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->scopeConfig->getValue(
            self::XML_HOST,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return (int) $this->scopeConfig->getValue(
            self::XML_PORT,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getInventoryFilename(): string
    {
        return $this->scopeConfig->getValue(
            self::XML_INVENTORY_FILENAME,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getInventorySource(): string
    {
        return $this->scopeConfig->getValue(
            self::XML_INVENTORY_SOURCE,
            ScopeInterface::SCOPE_STORE
        );
    }
}
