<?php
declare(strict_types=1);

namespace SixBySix\BeggRetailerFeed\Api\Service;

/**
 * Interface ConnectionInterface
 */
interface ConnectionInterface
{
    /**
     * Gets the file content from server
     *
     * @param string $filename
     * @return resource|false
     */
    public function download(string $filename);

    /**
     * Deletes file from server
     * @param string $filename
     * @return bool
     */
    public function delete(string $filename): bool;
}
