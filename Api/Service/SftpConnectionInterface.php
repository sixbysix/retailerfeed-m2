<?php
declare(strict_types=1);

namespace SixBySix\BeggRetailerFeed\Api\Service;

/**
 * Interface SftpConnectionInterface
 */
interface SftpConnectionInterface extends ConnectionInterface
{
}
