<?php
declare(strict_types=1);

namespace SixBySix\BeggRetailerFeed\Api\Helper;

/**
 * Interface ConfigHelperInterface
 */
interface ConfigHelperInterface
{
    public function isEnabled(): bool;
    public function getUsername(): string;
    public function getPassword(): string;
    public function getHost(): string;
    public function getPort(): int;
    public function getInventoryFilename(): string;
    public function getInventorySource(): string;
}
