<?php
declare(strict_types=1);

namespace SixBySix\BeggRetailerFeed\Api\Cron;

/**
 * Interface InventoryFeedImportCronInterface
 */
interface InventoryFeedImportCronInterface
{
    public function execute(): void;
}
