<?php
declare(strict_types=1);

namespace SixBySix\BeggRetailerFeed\Api\Data\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;


/**
 * Interface InventorySourceInterface
 */
interface InventorySourceInterface extends OptionSourceInterface
{
}
